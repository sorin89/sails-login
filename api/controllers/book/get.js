/**
 * Module dependencies
 */

// ...


/**
 * book/get.js
 *
 * Get book.
 */
module.exports = async function get(req, res) {

  Book.find({owner:req.me.id})
  	.exec(function(err, books) {

		  res.view({books:books});
  	});

};
