/**
 * Module dependencies
 */

// ...


/**
 * book/create.js
 *
 * Create book.
 */
module.exports = async function create(req, res) {
	var name = req.param('name');
	var author = req.param('author');
	Book.create({name:name, author:author, owner:req.me.id})
		.exec(function(err, book) {
			console.log(req.me.id);
		  sails.log.debug('create book');
		  console.log(req.me);
		  res.redirect('/books');
		});

};
